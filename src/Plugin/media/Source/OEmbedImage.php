<?php

namespace Drupal\media_oembed_image\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\MediaTypeInterface;
use Drupal\media\Plugin\media\Source\OEmbed;

/**
 * Provides a media source plugin for oEmbed image resources.
 *
 * @todo Extract an interface for the methods here which relate to preparing
 *   the alt text field? (e.g. called MediaSourceTextAlternativeInterface)
 */
class OEmbedImage extends OEmbed {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'alt_text_field' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * Get the alternative text field options for the media type form.
   *
   * This returns all string fields related to media entities.
   *
   * @return string[]
   *   A list of alternative text field options for the media type form.
   */
  protected function getAltTextFieldOptions() {
    // If there are existing fields to choose from, allow the user to reuse one.
    $options = [];
    foreach ($this->entityFieldManager->getFieldStorageDefinitions('media') as $field_name => $field) {
      $allowed_type = in_array($field->getType(), ['string'], TRUE);
      if ($allowed_type && !$field->isBaseField()) {
        $options[$field_name] = $field->getLabel();
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $alt_text_field_options = $this->getAltTextFieldOptions();
    $form['alt_text_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field with alternative text'),
      '#default_value' => $this->configuration['alt_text_field'],
      '#empty_option' => $this->t('- Create -'),
      '#options' => $alt_text_field_options,
      '#description' => $this->t('Select the field that will store a short alternative text for the image. If "Create" is selected a new field will be automatically created.'),
    ];

    if (!$alt_text_field_options && $form_state->get('operation') === 'add') {
      $form['alt_text_field']['#access'] = FALSE;
      $field_definition = $this->fieldTypeManager->getDefinition(reset($this->pluginDefinition['allowed_field_types']));
      $form['alt_text_field_message'] = [
        '#type' => 'item',
        '#markup' => $this->t('%field_type field will be automatically created on this type to store a short alternative text for the image.', [
          '%field_type' => $field_definition['label'],
        ]),
      ];
    }
    elseif ($form_state->get('operation') === 'edit') {
      $form['alt_text_field']['#access'] = FALSE;
      $fields = $this->entityFieldManager->getFieldDefinitions('media', $form_state->get('type')->id());
      $form['alt_text_field_message'] = [
        '#type' => 'item',
        '#markup' => $this->t('%field_name field is used to store a short alternative text for the image.', [
          '%field_name' => $fields[$this->configuration['alt_text_field']]->getLabel(),
        ]),
      ];
    }

    // The alt_text_field configuration should follow the source_field
    // configuration, so move the thumbnail and provider fields further down.
    $form['thumbnails_directory']['#weight'] = 10;
    $form['providers']['#weight'] = 20;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    // If no alt text field is explicitly set, create it now.
    if (empty($this->configuration['alt_text_field'])) {
      $field_storage = $this->createAltTextFieldStorage();
      $field_storage->save();
      $this->configuration['alt_text_field'] = $field_storage->getName();
    }
  }

  /**
   * Creates the alternative text field storage definition.
   *
   * @return \Drupal\field\FieldStorageConfigInterface
   *   The unsaved field storage definition.
   */
  protected function createAltTextFieldStorage() {
    return $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->create([
        'entity_type' => 'media',
        'field_name' => $this->getAltTextFieldName(),
        'type' => 'string',
      ]);
  }

  /**
   * Returns the text altertative field storage definition.
   *
   * @return \Drupal\Core\Field\FieldStorageDefinitionInterface|null
   *   The field storage definition or NULL if it doesn't exists.
   */
  protected function getAltTextFieldStorage() {
    // Nothing to do if no alternative text field is configured yet.
    $field = $this->configuration['alt_text_field'];
    if ($field) {
      // Even if we do know the name of the alternative text field, there's no
      // guarantee that it exists.
      $fields = $this->entityFieldManager->getFieldStorageDefinitions('media');
      return isset($fields[$field]) ? $fields[$field] : NULL;
    }
    return NULL;
  }

  /**
   * Creates the alternative text field definition for a type.
   *
   * @param \Drupal\media\MediaTypeInterface $type
   *   The media type.
   *
   * @return \Drupal\field\FieldConfigInterface
   *   The unsaved field definition.
   */
  public function createAltTextField(MediaTypeInterface $type) {
    $storage = $this->getAltTextFieldStorage() ?: $this->createAltTextFieldStorage();
    return $this->entityTypeManager
      ->getStorage('field_config')
      ->create([
        'field_storage' => $storage,
        'bundle' => $type->id(),
        'label' => $this->t('Alternative text'),
        'required' => TRUE,
      ])
      ->setDescription('Short description of the image used by screen readers and displayed when the image is not loaded. This is important for accessibility.');
  }

  /**
   * Determine the name of the alternative text field.
   *
   * @return string
   *   The alternative text field name. If one is already stored in
   *   configuration, it is returned. Otherwise, a new, unused one is generated.
   */
  protected function getAltTextFieldName() {
    // Some media sources are using a deriver, so their plugin IDs may contain
    // a separator (usually ':') which is not allowed in field names.
    $base_id = 'field_media_' . str_replace(static::DERIVATIVE_SEPARATOR, '_', $this->getPluginId()) . '_alt';
    $tries = 0;
    $storage = $this->entityTypeManager->getStorage('field_storage_config');

    // Iterate at least once, until no field with the generated ID is found.
    do {
      $id = $base_id;
      // If we've tried before, increment and append the suffix.
      if ($tries) {
        $id .= '_' . $tries;
      }
      $field = $storage->load('media.' . $id);
      $tries++;
    } while ($field);

    return $id;
  }

  /**
   * Get the alternative text field definition for a media type.
   *
   * @param \Drupal\media\MediaTypeInterface $type
   *   A media type.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|null
   *   The alternative text field definition, or NULL if it doesn't exist or has
   *   not been configured yet.
   *
   * @todo Can I avoid having to pass $type in? Can't recall why I needed to do it
   *   this way, and it doesn't seem very elegant.
   */
  public function getAltTextFieldDefinition(MediaTypeInterface $type) {
    // Nothing to do if no alternative text field is configured yet.
    $field = $this->configuration['alt_text_field'];
    if ($field) {
      // Even if we do know the name of the alternative text field, there is no
      // guarantee that it already exists.
      $fields = $this->entityFieldManager->getFieldDefinitions('media', $type->id());
      return isset($fields[$field]) ? $fields[$field] : NULL;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareFormDisplay(MediaTypeInterface $type, EntityFormDisplayInterface $display) {
    parent::prepareFormDisplay($type, $display);

    $source_field = $this->getSourceFieldDefinition($type)->getName();
    $alt_text_field = $this->getAltTextFieldDefinition($type)->getName();

    // Place the alternative text field just after the remote image URL field.
    $display->setComponent($alt_text_field, [
      'type' => 'string_textfield',
      'weight' => $display->getComponent($source_field)['weight'] + 1,
    ]);
  }

}
